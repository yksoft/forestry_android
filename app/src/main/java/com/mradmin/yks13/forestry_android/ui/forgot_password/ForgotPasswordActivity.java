package com.mradmin.yks13.forestry_android.ui.forgot_password;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mradmin.yks13.forestry_android.R;
import com.mradmin.yks13.forestry_android.ui.base.BaseActivity;
import com.mradmin.yks13.forestry_android.ui.reset_password.ResetPasswordActivity;
import com.mradmin.yks13.forestry_android.view.CustomTextInputLayout;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_back)
    ImageView backButton;

    @BindView(R.id.toolbar_title)
    TextView title;

    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.email_layout)
    CustomTextInputLayout emailLayout;

    @BindView(R.id.next)
    Button nextButton;

    @BindView(R.id.topCropImageView)
    ImageView background;

    @BindView(R.id.forgot_password_progress)
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        initViews();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

//    @Override
//    public void onDataReceived(Boolean success) {
//        if (success) {
//            goToNextAndFinish(ResetPasswordActivity.getIntent(this, email.getText().toString()));
//        }
//    }

//    @Override
//    public void onError(Throwable error) {
//        emailLayout.setError(getString(R.string.not_exist_email));
//    }
//
//    @Override
//    protected View getProgressView() {
//        return progress;
//    }

    @OnClick(R.id.next)
    public void onRestoreClick() {
        //TODO вернуть и поправить
        System.out.println("----------------- click next");
        goToNextAndFinish(ResetPasswordActivity.getIntent(this, email.getText().toString()));
        //if (ValidationUtils.isCorrectEmail(email.getText().toString())) {
        //    getViewModel().sendRestorePasswordRequest(email.getText().toString());
        //    emailLayout.setError(null);
       // } else {
        //    emailLayout.setError(getString(R.string.error_email));
       // }
    }

    @OnClick(R.id.toolbar_back)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initViews() {
        setSupportActionBar(toolbar);
        backButton.setVisibility(View.VISIBLE);
        title.setText(R.string.forgot);
        scrollView.post(() -> scrollView.scrollTo(0, scrollView.getHeight()));
        changeStatusBarColor(R.color.login_background);

        background.setImageResource(R.drawable.splash_screen_back);

        //TODO убрать
        nextButton.setEnabled(true);
    }
}
