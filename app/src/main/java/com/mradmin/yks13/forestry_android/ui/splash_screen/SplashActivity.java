package com.mradmin.yks13.forestry_android.ui.splash_screen;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.mradmin.yks13.forestry_android.R;
import com.mradmin.yks13.forestry_android.ui.base.BaseActivity;
import com.mradmin.yks13.forestry_android.ui.login.LoginActivity;
import com.mradmin.yks13.forestry_android.ui.main_screen.MainActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showSplashDialog();
    }

    private void showSplashDialog() {

        LayoutInflater inflater = getLayoutInflater();
        View textsView = inflater.inflate(R.layout.layout_splash_dialog, null);

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setCustomTitle(textsView)
                .setPositiveButton(R.string.splash_dialog_button_auth, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        auth();
                    }
                })
                .setNegativeButton(R.string.splash_dialog_button_guest, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        guest();
                    }
                })
                .create();

        alertDialog.show();
    }

    private void auth() {
        Intent to = new Intent(this, LoginActivity.class);

        Intent current = getIntent();
        String action = current.getAction();
        if (action != null && action.equals(Intent.ACTION_VIEW)) {
            to.setAction(Intent.ACTION_VIEW);
            to.setData(current.getData());
            to.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        openScreenAndFinish(to);
    }

    private void guest() {
        Intent to = new Intent(this, MainActivity.class);

        Intent current = getIntent();
        String action = current.getAction();
        if (action != null && action.equals(Intent.ACTION_VIEW)) {
            to.setAction(Intent.ACTION_VIEW);
            to.setData(current.getData());
            to.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        openScreenAndFinish(to);
    }
}
