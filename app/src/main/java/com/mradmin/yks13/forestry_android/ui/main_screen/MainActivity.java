package com.mradmin.yks13.forestry_android.ui.main_screen;

import android.content.res.Configuration;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mradmin.yks13.forestry_android.R;
import com.mradmin.yks13.forestry_android.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.item_maps)
    RelativeLayout maps;

    @BindView(R.id.item_topo_layers)
    RelativeLayout topoLayers;

    @BindView(R.id.tv_topo_layer_name)
    TextView tvTopoLayerName;

    @BindView(R.id.item_sync)
    RelativeLayout sync;

    @BindView(R.id.item_settings)
    RelativeLayout settings;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.exit)
    TextView exit;

    @BindView(R.id.drawer_background)
    ImageView drawerBackground;

    @BindView(R.id.avatar)
    ImageView avatar;

    @BindView(R.id.compass)
    ImageView compass;

//    @BindView(R.id.bottom_sheet_container)
//    BottomSheetContainer bottomSheetContainer;

    @BindView(R.id.ic_map)
    View mapIconView;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        changeFragment(MainFragment.class);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                setNoActionState();
            }
        };

        drawerLayout.addDrawerListener(drawerToggle);
        setupActionBar();
        //setupBottomSheet();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
        //closeBottomSheetContainer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    private void changeFragment(Class fClass) {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentByTag(fClass.getName());

        if (fragment == null) {
            try {
                fragment = (Fragment) fClass.newInstance();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (fragment != null) {
            fm.beginTransaction()
                    .replace(R.id.content_frame, fragment, fClass.getName())
                    .addToBackStack(null)
                    .commit();
        }
    }

    private void setupActionBar() {
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    private void setNoActionState() {
        //closeBottomSheetContainer();
        //getMapFragment().getViewModel().setNoActionState();
    }
}
