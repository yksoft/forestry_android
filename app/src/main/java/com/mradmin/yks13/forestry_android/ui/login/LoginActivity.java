package com.mradmin.yks13.forestry_android.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mradmin.yks13.forestry_android.R;
import com.mradmin.yks13.forestry_android.ui.base.BaseActivity;
import com.mradmin.yks13.forestry_android.ui.forgot_password.ForgotPasswordActivity;
import com.mradmin.yks13.forestry_android.ui.main_screen.MainActivity;
import com.mradmin.yks13.forestry_android.ui.register.RegisterActivity;
import com.mradmin.yks13.forestry_android.view.CustomTextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView title;

    @BindView(R.id.toolbar_back)
    ImageView backButton;

    @BindView(R.id.password)
    AppCompatEditText password;

    @BindView(R.id.login)
    EditText login;

    @BindView(R.id.topCropImageView)
    ImageView background;

    @BindView(R.id.next)
    Button submit;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.show_password)
    TextView showPassword;

    @BindView(R.id.login_layout)
    CustomTextInputLayout loginLayout;

    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @BindView(R.id.password_layout)
    CustomTextInputLayout passwordLayout;

    @BindView(R.id.login_container)
    RelativeLayout loginContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        hideHideButtonIfLollipop();

        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loginContainer.requestFocus();
    }

    private void hideHideButtonIfLollipop() {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP ||
                Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
            showPassword.setVisibility(View.INVISIBLE);
        }
    }

    private Boolean checkFieldsValid(CharSequence login, CharSequence password) {
        passwordLayout.setError("");
        boolean flag = true;

        if (TextUtils.isEmpty(password) || password.length() < RegisterActivity.PASSWORD_LENGTH) {
            flag = false;
        }

        if (login.toString().isEmpty()) {
            flag = false;
        }
        onChangeLoginStatus(flag);

        return flag;
    }

    private void onChangeLoginStatus(boolean flag) {
        if (!flag
                && !password.getText().toString().isEmpty()
                && !login.getText().toString().isEmpty()) {
            passwordLayout.setError(getString(R.string.email_or_pass_wrong));
            loginLayout.setError(" ");
        } else {
            passwordLayout.setError(null);
            loginLayout.setError(null);
        }
    }

    @OnClick(R.id.next)
    public void onLoginClick(View view) {
        //if (checkFields()) {
            //viewModel.login(
            //        login.getText().toString().trim(),
            //        password.getText().toString().trim());
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        //}
    }

    @OnTextChanged({R.id.login, R.id.password})
    public void onTextChanged() {
        loginLayout.setError(null);
        passwordLayout.setError(null);
        if (login.getText().toString().isEmpty()
                || password.getText().toString().isEmpty()) {
            submit.setEnabled(false);
        } else {
            if (checkFieldsValid(login.getText(), password.getText())) {
                submit.setEnabled(true);
            }
        }
    }

    private boolean checkFields() {
        return checkFieldsValid(login.getText(), password.getText());
    }

    private void startNextScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }

    private void initViews() {
        setSupportActionBar(toolbar);
        backButton.setVisibility(View.VISIBLE);
        title.setText(R.string.authorization);
        scrollView.post(() -> scrollView.scrollTo(0, scrollView.getHeight()));

        background.setImageResource(R.drawable.splash_screen_background);
        changeStatusBarColor(R.color.login_background);
        password.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onLoginClick(v);
                return true;
            }

            return false;
        });
    }

    private void changePasswordVisibility(Boolean isVisible) {
        if (isVisible) {
            int selection = password.getSelectionEnd();

            password.setTransformationMethod(null);
            showPassword.setText(R.string.hide_pass);

            password.setSelection(selection);
        } else {
            int selection = password.getSelectionEnd();

            password.setTransformationMethod(new PasswordTransformationMethod());
            showPassword.setText(R.string.show_pass);

            password.setSelection(selection);
        }
    }

    @OnClick(R.id.show_password)
    public void showPassword() {
        //TODO
    }

    @OnClick(R.id.registration)
    public void openRegistrationActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.forgot_password)
    public void onForgotPasswordClick() {
        openScreen(ForgotPasswordActivity.class);
    }

    @OnClick(R.id.toolbar_back)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
