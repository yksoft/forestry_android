package com.mradmin.yks13.forestry_android.view;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordTextInputEditText extends AppCompatEditText {

    private PasswordState state = PasswordState.NONE;
    private OnStateChangedListener listener;

    public PasswordTextInputEditText(Context context) {
        super(context);
        prepareView();
    }

    public PasswordTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        prepareView();
    }

    public PasswordTextInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        prepareView();
    }

    public void setStateChangeListener(OnStateChangedListener listener) {
        this.listener = listener;
    }

    private void prepareView() {
        addTextWatcher();
    }

    private void addTextWatcher() {
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateState(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void updateState(String newValue) {
        if (TextUtils.isEmpty(newValue)) {
            state = PasswordState.NONE;
        } else {
            if (newValue.length() < 8) {
                state = PasswordState.WEAK;
            }

            if (newValue.length() >= 8
                    && (isDigitsOnly(newValue)
                    || isAlphabetsOnly(newValue)
                    || isOneCase(newValue)
                    || isSpecialCharactersOnly(newValue))) {
                state = PasswordState.MEDIUM;
            }

            if (newValue.length() >= 8) {
                int count = 0;

                if (containsDigits(newValue)) {
                    count++;
                }

                if (containsAlphabets(newValue)) {
                    count++;
                }

                if (!isOneCase(newValue)) {
                    count++;
                }

                if (containsSpecialCharacters(newValue)) {
                    count++;
                }

                if (count > 1) {
                    state = PasswordState.STRONG;
                }
            }
        }


        dispatchState();
    }

    private boolean isDigitsOnly(String value) {
        return TextUtils.isDigitsOnly(value);
    }

    private boolean isAlphabetsOnly(String value) {
        return value.matches("[\\p{Alpha}]*$");
    }

    private boolean isOneCase(String value) {
        return value.equals(value.toLowerCase()) || value.equals(value.toUpperCase());
    }

    private boolean isSpecialCharactersOnly(String value) {
        return value.matches("[\\p{Punct}]*$");
    }

    private boolean containsDigits(String value) {
        Pattern pattern = Pattern.compile("[0-9]");
        Matcher matcher = pattern.matcher(value);
        return matcher.find();
    }

    private boolean containsAlphabets(String value) {
        Pattern pattern = Pattern.compile("[a-zA-Z]");
        Matcher matcher = pattern.matcher(value);
        return matcher.find();
    }

    private boolean containsSpecialCharacters(String value) {
        Pattern pattern = Pattern.compile("[\\p{Punct}]");
        Matcher matcher = pattern.matcher(value);
        return matcher.find();
    }

    private void dispatchState() {
        if (listener != null) {
            listener.onStateChanged(state);
        }
    }

    public enum PasswordState {
        NONE,
        WEAK,
        MEDIUM,
        STRONG
    }

    public interface OnStateChangedListener {
        void onStateChanged(PasswordState state);
    }
}
