package com.mradmin.yks13.forestry_android.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.animation.Animation;
import android.widget.Toast;

public class BaseFragment extends Fragment {

    protected void showToast(int stringId) {
        if (!isVisible()) return;

        showToast(stringId, Toast.LENGTH_SHORT);
    }

    protected void showToast(String text) {
        if (!isVisible()) return;

        showToast(text, Toast.LENGTH_SHORT);
    }

    protected void showToast(int stringId, int toastLength) {
        if (!isVisible()) return;

        Toast.makeText(getActivity(), stringId, toastLength).show();
    }

    protected void showToast(String text, int toastLength) {
        if (!isVisible()) return;

        Toast.makeText(getActivity(), text, toastLength).show();
    }

    protected void goToNext(Class clazz) {
        startActivity(new Intent(getActivity(), clazz));
    }

    protected void goToNextAndFinish(Class clazz) {
        goToNext(clazz);
        getActivity().finish();
    }

    protected void goToNextForResult(Class clazz, int requestCode) {
        startActivityForResult(new Intent(getActivity(), clazz), requestCode);
    }

    protected void goToNextForResult(Class clazz, int requestCode, Bundle bundle) {
        final Intent intent = new Intent(getActivity(), clazz);
        intent.putExtra(BaseActivity.BUNDLE, bundle);
        startActivityForResult(intent, requestCode);
    }

    public void openScreen(Intent intent) {
        startActivity(intent);
    }

    public void openScreenAndFinish(Intent intent) {
        startActivity(intent);
        getActivity().finish();
    }
}

