package com.mradmin.yks13.forestry_android.api;

import com.mradmin.yks13.forestry_android.model.ClusterResponse;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {

    @Multipart
    @POST("/api/feedback")
    Call<ResponseBody> sendOffenseMessage(@Part MultipartBody.Part type,
                                          @Part MultipartBody.Part message,
                                          @Part MultipartBody.Part image,
                                          @Part MultipartBody.Part lat,
                                          @Part MultipartBody.Part lon);

    @GET("/api/cluster")
    Call<ClusterResponse> getCluster();
}
