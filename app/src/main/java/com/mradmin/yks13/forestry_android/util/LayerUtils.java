package com.mradmin.yks13.forestry_android.util;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;

import com.google.android.gms.maps.GoogleMap;
import com.google.maps.android.data.geojson.GeoJsonFeature;
import com.google.maps.android.data.geojson.GeoJsonLayer;
import com.google.maps.android.data.geojson.GeoJsonPolygonStyle;
import com.mradmin.yks13.forestry_android.model.MapLayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class LayerUtils {

    public static JSONObject getJSONObject(Context context) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(loadJSONFromAssets(context));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public static GeoJsonLayer convertLayer(GoogleMap map, JSONObject jsonObject) {
        GeoJsonLayer geoJsonLayer = new GeoJsonLayer(map, jsonObject);
        //for (GeoJsonFeature feature : geoJsonLayer.getFeatures()) {
        //    feature.setProperty("z-index", layer.getLayoutsEntity().getZIndex());
        //    feature.setProperty("layer-type-id", layer.getWetlandsEntity().getId());
        //}
        addColorsToMarkers(geoJsonLayer, "#323232");


        return geoJsonLayer;
    }

    public static List<MapLayer> createMapLayers(JSONObject jsonObject) throws JSONException {

        List<MapLayer> layers = new ArrayList<>();

        JSONArray jsonArray = jsonObject.getJSONArray("features");
        int length = jsonArray.length();

        for (int i = 0; i < length; i++) {
            JSONObject feature = (JSONObject) jsonArray.get(i);
            JSONObject properties = feature.getJSONObject("properties");

            try {
                layers.add(createMapLayer(properties));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return layers;
    }

    // Private methods
    private static MapLayer createMapLayer(JSONObject feature) throws Exception {
        MapLayer layer = new MapLayer();
        String name;
        String district = "";

        if (feature.has("district")) {
            district = feature.getString("district");
        } else if (feature.has("district f")) {
            district = feature.getString("district f");
        } else  if (feature.has("forestry")) {
            district = feature.getString("forestry");
        }

        if (feature.has("name")) {
            name = feature.getString("name");
        } else if (feature.has("NN")){
            name = feature.getString("NN");
        } else {
            name = "empty name";
        }

        layer.setName(name);
        layer.setDistrict(district);

        //JSONArray coordinates = feature.getJSONObject("center").getJSONArray("coordinates");

        //layer.setId(MapLayer.createId(coordinates.get(0).toString(),
        //        coordinates.get(1).toString()));

        //Double latitude = Double.parseDouble(coordinates.get(0).toString());
        //Double longitude = Double.parseDouble(coordinates.get(1).toString());
        //List<Double> center = new ArrayList<>();

        //center.add(latitude);
        //center.add(longitude);

        //layer.setCenter(center);

        return layer;
    }

    private static void addColorsToMarkers(GeoJsonLayer layer, String color) {
        for (GeoJsonFeature feature : layer.getFeatures()) {
            GeoJsonPolygonStyle polygonStyle = new GeoJsonPolygonStyle();
            if (!TextUtils.isEmpty(color)) {
                int parseColor = Color.parseColor(color);
                int alphaColor = Color.argb(76, Color.red(parseColor), Color.green(parseColor), Color.blue(parseColor));
                polygonStyle.setFillColor(alphaColor);
            }
            if (feature.hasProperty("z-index"))
                polygonStyle.setZIndex(Float.parseFloat(feature.getProperty("z-index")));

            polygonStyle.setStrokeWidth(2);
            feature.setPolygonStyle(polygonStyle);
        }
    }

    private static String loadJSONFromAssets(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("geoJsonData.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }
}
