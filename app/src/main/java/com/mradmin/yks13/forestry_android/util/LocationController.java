package com.mradmin.yks13.forestry_android.util;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;

public class LocationController
        implements OnCompleteListener<Void> {
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 0;

    private Context context;
    private List<OnLocationChangedListener> listeners = new ArrayList<>();
    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private LatLng lastLocation;
    private LocationSettingsRequest locationSettingsRequest;
    private boolean requestingLocationUpdates;

    public LocationController(Context context) {
        this.context = context;
        createLocationApi();
        createLocationRequest();
        requestLastLocation();
    }

    public void addListener(OnLocationChangedListener listener) {
        listeners.add(listener);
        updateLocationState();
    }

    public void removeListener(OnLocationChangedListener listener) {
        listeners.remove(listener);
        updateLocationState();
    }

    public LatLng getLastLocation() {
        return lastLocation;
    }

    private void createLocationApi() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                lastLocation = new LatLng(locationResult.getLastLocation().getLatitude(),
                        locationResult.getLastLocation().getLongitude());
                dispatchNewLocation(lastLocation);
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
            }
        };
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @SuppressWarnings({"MissingPermission"})
    private void requestLastLocation() {
        if (PermissionUtils.hasLocationPermission(context)) {
            fusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                lastLocation = new LatLng(task.getResult().getLatitude(),
                                        task.getResult().getLongitude());
                                dispatchNewLocation(lastLocation);
                            } else {
                                Log.w("TAG", "Failed to get location.");
                            }
                        }
                    });
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void stopLocationUpdates() {
        if (!PermissionUtils.hasLocationPermission(context) || !requestingLocationUpdates) {
            return;
        }

        fusedLocationClient.removeLocationUpdates(locationCallback)
                .addOnCompleteListener(this);
    }

    @SuppressWarnings({"MissingPermission"})
    private void startLocationUpdates() {
        if (!PermissionUtils.hasLocationPermission(context)) {
            return;
        }

        requestingLocationUpdates = true;
        fusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback, Looper.myLooper());
    }

    private void updateLocationState() {
        if (listeners.size() > 0) {
            startLocationUpdates();
        } else {
            stopLocationUpdates();
        }
    }

    private void dispatchNewLocation(LatLng lastLocation) {
        Log.d("TAG", "new location called");

        for (OnLocationChangedListener listener : listeners) {
            listener.onLocationChanged(lastLocation);
        }
    }

    @Override
    public void onComplete(@NonNull Task<Void> task) {

    }

    public interface OnLocationChangedListener {
        void onLocationChanged(LatLng location);
    }
}
