package com.mradmin.yks13.forestry_android.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class CustomTextInputLayout extends TextInputLayout {
    public CustomTextInputLayout(Context context) {
        super(context);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

//    private void setFont() {
//        post(() -> {
//            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),
//                    "fonts/Roboto-Regular.ttf");
//            setTypeface(typeface);
//        });
//    }

    @Override
    public void addView(View child, int index, final ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        //setFont();

        if (child instanceof EditText) {
            EditText editText = (EditText) child;

            editText.setOnFocusChangeListener((v, hasFocus) -> {
                EditText text = (EditText) v;

                if (hasFocus) {
                    text.setSelection(text.getText().length());
                } else {
                    text.setSelection(0);
                }
            });

            editText.setOnEditorActionListener((v, actionId, event) -> {
                ((EditText) v).setSelection(0);
                return false;
            });
        }
    }
}
