package com.mradmin.yks13.forestry_android.ui.register;

import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mradmin.yks13.forestry_android.R;
import com.mradmin.yks13.forestry_android.ui.base.BaseActivity;
import com.mradmin.yks13.forestry_android.ui.main_screen.MainActivity;
import com.mradmin.yks13.forestry_android.view.CustomTextInputLayout;
import com.mradmin.yks13.forestry_android.view.NextButton;
import com.mradmin.yks13.forestry_android.view.PasswordTextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class RegisterActivity extends BaseActivity {

    public static final int PASSWORD_LENGTH = 1;

    @BindView(R.id.toolbar_title)
    TextView title;

    @BindView(R.id.toolbar_back)
    ImageView backButton;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.password)
    PasswordTextInputEditText password;

    @BindView(R.id.repeat)
    EditText repeat;

    @BindView(R.id.next)
    NextButton next;

    @BindView(R.id.show_password)
    TextView showPassword;

    @BindView(R.id.password_state)
    TextView passwordState;

    @BindView(R.id.login_layout)
    CustomTextInputLayout loginLayout;

    @BindView(R.id.email_layout)
    CustomTextInputLayout emailLayout;

    @BindView(R.id.license)
    CheckBox checkBox;

    @BindView(R.id.register_policy)
    TextView registerPolicy;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.repeat_layout)
    CustomTextInputLayout repeatLayout;

    @BindView(R.id.scrollView)
    NestedScrollView scrollView;

    private PasswordTextInputEditText.OnStateChangedListener stateChangedListener
            = new PasswordTextInputEditText.OnStateChangedListener() {
        @Override
        public void onStateChanged(PasswordTextInputEditText.PasswordState state) {
            switch (state) {
                case NONE:
                    passwordState.setText("");
                    break;

                case WEAK:
                    passwordState.setText(R.string.state_weak);
                    passwordState.setTextColor(
                            ContextCompat.getColor(RegisterActivity.this, R.color.state_red));
                    break;

                case MEDIUM:
                    passwordState.setText(R.string.state_medium);
                    passwordState.setTextColor(
                            ContextCompat.getColor(RegisterActivity.this, R.color.state_orange));
                    break;

                case STRONG:
                    passwordState.setText(R.string.state_strong);
                    passwordState.setTextColor(
                            ContextCompat.getColor(RegisterActivity.this, R.color.state_green));
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        initViews();

    }

//    @Override
//    public void onDataReceived(Boolean data) {
//        if (data) {
//            showToast(getString(R.string.register_success));
//            startNextScreen();
//        }
//    }

    private void initViews() {
        setSupportActionBar(toolbar);
        backButton.setVisibility(View.VISIBLE);
        title.setText(R.string.register);
        password.setStateChangeListener(stateChangedListener);
    }

    private boolean checkFieldsValid(CharSequence login,
                                     CharSequence email,
                                     CharSequence password,
                                     CharSequence repeat,
                                     Boolean isChecked) {
        if (TextUtils.isEmpty(login)) {
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            return false;
        }

        if (TextUtils.isEmpty(repeat)) {
            return false;
        }

        if (!password.toString().equals(repeat.toString())) {
            return false;
        }

        //if (!ValidationUtils.isCorrectEmail(email.toString())) {
        //    return false;
        //}

        if (!isChecked) {
            return false;
        }

        return true;
    }

    @OnClick(R.id.next)
    public void onRegisterClick() {
        //TODO раскомментить и поправить
        startNextScreen();
//        UserCredentials userCredentials = new UserCredentials(
//                email.getText().toString(),
//                password.getText().toString(),
//                name.getText().toString(),
//                checkBox.isChecked());
//
//        getViewModel().register(userCredentials);
    }

    @OnClick(R.id.toolbar_back)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.show_password)
    public void showPassword() {
        //getViewModel().changePasswordVisibility();
    }

    @OnClick(R.id.checkbox_container)
    public void onLicenseChanged() {
        //getViewModel().changeLicense();
    }

    private void changePasswordVisibility(Boolean isVisible) {
        if (isVisible) {
            int passwordSelection = password.getSelectionEnd();
            int repeatSelection = repeat.getSelectionEnd();

            password.setTransformationMethod(null);
            repeat.setTransformationMethod(null);
            showPassword.setText(R.string.hide_pass);

            password.setSelection(passwordSelection);
            repeat.setSelection(repeatSelection);
        } else {
            int passwordSelection = password.getSelectionEnd();
            int repeatSelection = repeat.getSelectionEnd();

            password.setTransformationMethod(new PasswordTransformationMethod());
            repeat.setTransformationMethod(new PasswordTransformationMethod());
            showPassword.setText(R.string.show_pass);

            password.setSelection(passwordSelection);
            repeat.setSelection(repeatSelection);
        }
    }

    private void startNextScreen() {
        startWithClearAndFinish(MainActivity.class);
    }

    private void setEmailCorrect(Boolean isCorrect) {
        if (isCorrect) {
            emailLayout.setError(null);
        } else {
            emailLayout.setError(getString(R.string.error_email));
        }
    }

    @OnTextChanged(R.id.email)
    public void emailChanged() {
        //if (getViewModel().getIsEmailCorrect().getValue()) {
        //    emailLayout.setError(null);
        //}
    }

    private Boolean isPasswordsSimilar(CharSequence mainPassword, CharSequence repeatedPassword) {
        String stringPassword = mainPassword.toString();
        String stringRepeatedPassword = repeatedPassword.toString();

        if ((!stringPassword.equals(stringRepeatedPassword))
                && (!stringPassword.isEmpty())
                && (!stringRepeatedPassword.isEmpty())) {
            repeatLayout.setError(getString(R.string.not_match));
        } else {
            repeatLayout.setError(null);
        }

        return stringPassword.equals(stringRepeatedPassword);
    }

    private void setLicenseChecked(Boolean isChecked) {
        checkBox.setChecked(isChecked);
    }
}
