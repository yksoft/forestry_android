package com.mradmin.yks13.forestry_android.repository;

import android.content.Context;

import com.mradmin.yks13.forestry_android.MainApplication;
import com.mradmin.yks13.forestry_android.api.ApiService;
import com.mradmin.yks13.forestry_android.model.ClusterResponse;
import com.mradmin.yks13.forestry_android.model.OffenseMessage;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ApiRepository {

    private Context context;
    private ApiService apiService;

    public ApiRepository(Context context) {
        this.context = context;
        if (apiService == null)
            apiService = MainApplication.getRetrofit().create(ApiService.class);
    }

    public void sendOffenseMessage(Callback<ResponseBody> callback, OffenseMessage offenseMessage) {

        RequestBody requestFile = RequestBody.create(MediaType.parse("image"), offenseMessage.getImage());
        MultipartBody.Part bodyImage = MultipartBody.Part.createFormData("image", "image.jpeg", requestFile);
        MultipartBody.Part bodyType = MultipartBody.Part.createFormData("type", offenseMessage.getType().toString());
        MultipartBody.Part bodyMessage = MultipartBody.Part.createFormData("message", offenseMessage.getMessage());
        MultipartBody.Part bodyLat = MultipartBody.Part.createFormData("lat", offenseMessage.getLat().toString());
        MultipartBody.Part bodyLon = MultipartBody.Part.createFormData("lon", offenseMessage.getLon().toString());

        Call<ResponseBody> responseCall = apiService.sendOffenseMessage(bodyType, bodyMessage, bodyImage, bodyLat, bodyLon);

        responseCall.enqueue(callback);
    }

    public void getCluster(Callback<ClusterResponse> callback) {

        Call<ClusterResponse> responseCall = apiService.getCluster();

        responseCall.enqueue(callback);
    }
}
