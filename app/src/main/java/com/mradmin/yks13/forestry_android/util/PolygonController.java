package com.mradmin.yks13.forestry_android.util;

import android.content.Context;
import android.util.SparseArray;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.geojson.GeoJsonFeature;
import com.google.maps.android.data.geojson.GeoJsonLayer;
import com.google.maps.android.data.geojson.GeoJsonMultiPolygon;
import com.google.maps.android.data.geojson.GeoJsonPolygon;

import java.util.List;

public class PolygonController
        implements LocationController.OnLocationChangedListener {
    private static final int NOTIFICATION_OUT_HUNTING_MAP = 2048;
    private static final int NOTIFICATION_IN_HUNTING_MAP = 2049;

    private Context context;
    private SparseArray<GeoJsonFeature> polygonsHash = new SparseArray<>();
    private GeoJsonFeature currentHuntingMap = null;
    private boolean skipNextCheck = false;
    private LatLng location = null;

    public PolygonController(Context context) {
        this.context = context;
    }

    @Override
    public void onLocationChanged(LatLng location) {
        this.location = location;
    }

    public Feature getFeature(int hashCode) {
        return polygonsHash.get(hashCode);
    }

    public void addFeatures(GeoJsonLayer layer) {
        for (GeoJsonFeature feature : layer.getFeatures()) {
            Object g = feature.getGeometry();
            if (g instanceof GeoJsonPolygon) {
                GeoJsonPolygon pol = (GeoJsonPolygon) g;
                for (List<LatLng> coordinates : pol.getCoordinates()) {
                    polygonsHash.put(coordinates.hashCode(), feature);
                }
            } else if (g instanceof GeoJsonMultiPolygon) {
                GeoJsonMultiPolygon multiPol = (GeoJsonMultiPolygon) g;
                for (GeoJsonPolygon pol : multiPol.getPolygons()) {
                    for (List<LatLng> coordinates : pol.getCoordinates()) {
                        polygonsHash.put(coordinates.hashCode(), feature);
                    }
                }
            }
        }
    }

    public void removeFeatures(GeoJsonLayer layer) {
        for (GeoJsonFeature feature : layer.getFeatures()) {
            Object g = feature.getGeometry();
            if (g instanceof GeoJsonPolygon) {
                GeoJsonPolygon pol = (GeoJsonPolygon) g;
                for (List<LatLng> coordinates : pol.getCoordinates()) {
                    polygonsHash.remove(coordinates.hashCode());
                }
            } else if (g instanceof GeoJsonMultiPolygon) {
                GeoJsonMultiPolygon multiPol = (GeoJsonMultiPolygon) g;
                for (GeoJsonPolygon pol : multiPol.getPolygons()) {
                    for (List<LatLng> coordinates : pol.getCoordinates()) {
                        polygonsHash.remove(coordinates.hashCode());
                    }
                }
            }
        }
    }

    public void removeFeatures() {
        for (int i = 0; i < polygonsHash.size(); i++) {
            polygonsHash.remove(polygonsHash.keyAt(i));
        }
    }

    private GeoJsonFeature getNewFeature(GeoJsonFeature feature, LatLng location,
                                         List<? extends List<LatLng>> coordinates) {
        GeoJsonFeature newFeature = null;

        for (List<LatLng> points : coordinates) {
            if (PolyUtil.containsLocation(location, points, true)) {
                if (newFeature != null) {
                    int currentZ = Integer.parseInt(feature.getProperty("z-index"));
                    int newZ = Integer.parseInt(newFeature.getProperty("z-index"));

                    if (currentZ > newZ) {
                        newFeature = feature;
                    }
                } else {
                    newFeature = feature;
                }
            }
        }

        return newFeature;
    }
}

