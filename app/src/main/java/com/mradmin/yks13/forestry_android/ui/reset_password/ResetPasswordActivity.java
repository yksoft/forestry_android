package com.mradmin.yks13.forestry_android.ui.reset_password;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mradmin.yks13.forestry_android.R;
import com.mradmin.yks13.forestry_android.ui.base.BaseActivity;
import com.mradmin.yks13.forestry_android.ui.main_screen.MainActivity;
import com.mradmin.yks13.forestry_android.view.CustomTextInputLayout;
import com.mradmin.yks13.forestry_android.view.NextButton;
import com.mradmin.yks13.forestry_android.view.PasswordTextInputEditText;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ResetPasswordActivity extends BaseActivity {

    public static final String BUNDLE_EMAIL = "bundle_email";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_back)
    ImageView backButton;

    @BindView(R.id.toolbar_title)
    TextView title;

    @BindView(R.id.reset_password_progress)
    ProgressBar progress;

    @BindView(R.id.code)
    EditText code;

    @BindView(R.id.password)
    PasswordTextInputEditText password;

    @BindView(R.id.show_password)
    TextView showPassword;

    @BindView(R.id.repeat)
    EditText repeat;

    @BindView(R.id.topCropImageView)
    ImageView background;

    @BindView(R.id.next)
    NextButton next;

    @BindView(R.id.repeat_layout)
    CustomTextInputLayout repeatPasswordLayout;

    @BindView(R.id.code_layout)
    CustomTextInputLayout codeLayout;

    private String email;

    public static Intent getIntent(Context context, String email) {
        Intent intent = new Intent(context, ResetPasswordActivity.class);
        intent.putExtra(BUNDLE_EMAIL, email);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);

        email = getIntent().getExtras().getString(BUNDLE_EMAIL);

        hideHideButtonIfLollipop();

        initViews();
    }

//    @Override
//    public void onDataReceived(Boolean isSuccess) {
//        if (isSuccess) {
//            goToNextAndFinish(MainActivity.class);
//            showToast(R.string.success_changed);
//        }
//    }

//    @Override
//    public void onError(Throwable error) {
//        codeLayout.setError(getString(R.string.wrong_code));
//    }

    @OnClick(R.id.next)
    public void onNextClicked() {
        //TODO раскомментить и поправить
        goToNextAndFinish(MainActivity.class);
//        getViewModel().requestChangePassword(email, code.getText().toString(),
//                password.getText().toString());
    }

    @OnClick(R.id.toolbar_back)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.show_password)
    public void showPassword() {
        //getViewModel().changePasswordVisibility();
    }

//    @Override
//    protected View getProgressView() {
//        return progress;
//    }

    private void changePasswordVisibility(Boolean isVisible) {
        if (isVisible) {
            int passwordSelection = password.getSelectionEnd();
            int repeatSelection = repeat.getSelectionEnd();

            password.setTransformationMethod(null);
            repeat.setTransformationMethod(null);
            showPassword.setText(R.string.hide_pass);

            password.setSelection(passwordSelection);
            repeat.setSelection(repeatSelection);
        } else {
            int passwordSelection = password.getSelectionEnd();
            int repeatSelection = repeat.getSelectionEnd();

            password.setTransformationMethod(new PasswordTransformationMethod());
            repeat.setTransformationMethod(new PasswordTransformationMethod());
            showPassword.setText(R.string.show_pass);

            password.setSelection(passwordSelection);
            repeat.setSelection(repeatSelection);
        }
    }

    private boolean checkFieldsValid(CharSequence code,
                                     CharSequence password,
                                     CharSequence repeat) {
        if (TextUtils.isEmpty(code) || code.length() != 4) {
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            return false;
        }

        if (TextUtils.isEmpty(repeat)) {
            return false;
        }

        if (!password.toString().equals(repeat.toString())) {
            return false;
        }

        return true;
    }

    private void hideHideButtonIfLollipop() {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP ||
                Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
            showPassword.setVisibility(View.INVISIBLE);
        }
    }

    private void initViews() {
        setSupportActionBar(toolbar);
        backButton.setVisibility(View.VISIBLE);
        title.setText(R.string.forgot);

        background.setImageResource(R.drawable.splash_screen_back);

        //TODO убрать
        next.setEnabled(true);
    }

    private Boolean isPasswordsSimilar(Boolean correct) {
        if (correct) {
            repeatPasswordLayout.setError(null);
        } else {
            repeatPasswordLayout.setError(getString(R.string.passwords_are_not_simmilar));
        }
        return correct;
    }

    @OnTextChanged(R.id.code)
    public void codeChanged() {
        codeLayout.setError(null);
    }

//    private void isCodeCorrect(Boolean correct) {
//        if (correct) {
//            codeLayout.setError(null);
//        } else {
//            codeLayout.setError(getString(R.string.wrong_reset_code));
//        }
//    }
}
