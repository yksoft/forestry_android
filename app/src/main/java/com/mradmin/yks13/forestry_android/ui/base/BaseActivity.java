package com.mradmin.yks13.forestry_android.ui.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public final static String BUNDLE = "bundle_";

    protected void showToast(int stringId) {
        showToast(stringId, Toast.LENGTH_SHORT);
    }

    protected void showToast(int stringId, int toastLength) {
        Toast.makeText(this, stringId, toastLength).show();
    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected boolean hasFragment(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag) != null;
    }

    protected void openScreen(Class clazz) {
        startActivity(new Intent(this, clazz));
    }

    protected void openScreenAndFinish(Class clazz) {
        startActivity(new Intent(this, clazz));
        finish();
    }

    protected void openScreenAndFinish(Intent intent) {
        startActivity(intent);
        finish();
    }

    protected void openScreen(Intent intent) {
        startActivity(intent);
    }

    protected void goToNextAndFinish(Class clazz) {
        openScreen(clazz);
        finish();
    }

    protected void goToNextAndFinish(Intent intent) {
        openScreen(intent);
        finish();
    }

    protected void startWithClearAndFinish(Class clazz) {
        Intent intent = new Intent(this, clazz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    protected void goToNextForResult(Class clazz, int requestCode) {
        startActivityForResult(new Intent(this, clazz), requestCode);
    }

    protected void goToNextForResult(Class clazz, int requestCode, Bundle bundle) {
        final Intent intent = new Intent(this, clazz);
        intent.putExtra(BUNDLE, bundle);
        startActivityForResult(intent, requestCode);
    }

    protected void sendResult(int resultCode, Intent intent) {
        setResult(resultCode, intent);
        finish();
    }

    protected void changeStatusBarColor(@ColorRes int color) {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, color));
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyBoard();
    }

    protected void hideKeyBoard() {
        android.view.View currentFocus = getCurrentFocus();

        if (currentFocus == null) {
            return;
        }

        InputMethodManager imm
                = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }
}
