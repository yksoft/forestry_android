package com.mradmin.yks13.forestry_android;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainApplication extends Application {

    private static Retrofit retrofit;

    private static MainApplication _instance;

    public static MainApplication getInstance() {
        if (_instance == null)
            _instance = new MainApplication();

        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initRetrofit();

    }

    private void initRetrofit() {
        Gson gson = new GsonBuilder().setLenient().create();

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        httpClientBuilder.connectTimeout(2, TimeUnit.MINUTES);
        httpClientBuilder.writeTimeout(2, TimeUnit.MINUTES);
        httpClientBuilder.readTimeout(2, TimeUnit.MINUTES);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder.interceptors().add(interceptor);


        OkHttpClient httpClient = httpClientBuilder.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }
}
