package com.mradmin.yks13.forestry_android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.WindowInsets;
import android.widget.RelativeLayout;

public class CustomRelativeLayout extends RelativeLayout {
    private int[] insets = new int[4];

    public CustomRelativeLayout(Context context) {
        super(context);
    }

    public CustomRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public final int[] getInsets() {
        return insets;
    }

    @Override
    public final WindowInsets onApplyWindowInsets(WindowInsets insets) {
        this.insets[0] = insets.getSystemWindowInsetLeft();
        this.insets[1] = insets.getSystemWindowInsetTop();
        this.insets[2] = insets.getSystemWindowInsetRight();
        return super.onApplyWindowInsets(insets.replaceSystemWindowInsets(0, 0, 0,
                insets.getSystemWindowInsetBottom()));
    }
}
