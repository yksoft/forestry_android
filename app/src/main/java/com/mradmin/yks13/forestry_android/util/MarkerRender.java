package com.mradmin.yks13.forestry_android.util;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.mradmin.yks13.forestry_android.R;
import com.mradmin.yks13.forestry_android.model.ClusterData;

import static com.mradmin.yks13.forestry_android.model.OffenseType.fire;
import static com.mradmin.yks13.forestry_android.model.OffenseType.forest;
import static com.mradmin.yks13.forestry_android.model.OffenseType.trash;

public class MarkerRender extends DefaultClusterRenderer<ClusterData> {
    private Integer pointId;
    private float zoom;
    private ClusterManager<ClusterData> clusterManager;

    public MarkerRender(Context context, GoogleMap map, ClusterManager<ClusterData> clusterManager,
                        Integer pointId) {
        super(context, map, clusterManager);
        this.zoom = map.getCameraPosition().zoom;
        this.clusterManager = clusterManager;
        this.pointId = pointId;
    }

    public void setNavigateTo(int point) {
        ClusterData previousPoint = findMarker(pointId);
        pointId = point;
        drawItem(previousPoint);

        ClusterData marker = findMarker(pointId);
        drawItem(marker);
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    @Override
    protected void onClusterItemRendered(ClusterData point, Marker marker) {
        super.onClusterItemRendered(point, marker);
        marker.setTitle(String.valueOf(point.getId()));
        if (pointId != null && pointId == point.getId()) {
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_place));
        } else {
            switch (point.getType()) {
                case fire: {
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_red));
                    break;
                }
                case forest: {
                    if (point.isLegal() == 1)
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_green));
                    else
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_yellow));
                    break;
                }
                case trash: {
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_grey));
                    break;
                }
                default:
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker_red));
                    break;

            }
        }
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<ClusterData> cluster) {
        return cluster.getSize() > 1 && zoom < 20;
    }

    private ClusterData findMarker(Integer markerId) {
        ClusterData previousPoint = null;

        for (ClusterData pointEntity : clusterManager.getAlgorithm().getItems()) {
            if (markerId != null && markerId == pointEntity.getId()) {
                previousPoint = pointEntity;
            }
        }

        return previousPoint;
    }

    private void drawItem(ClusterData markerItem) {
        if (markerItem == null) {
            return;
        }

        Marker marker = getMarker(markerItem);

        if (marker != null) {
            onClusterItemRendered(markerItem, marker);
        }
    }
}
