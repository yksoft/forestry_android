package com.mradmin.yks13.forestry_android.model;

import java.util.List;

public class ClusterResponse {
    private boolean result;
    private List<ClusterData> data;

    public ClusterResponse() {
    }

    public ClusterResponse(boolean result, List<ClusterData> data) {
        this.result = result;
        this.data = data;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public List<ClusterData> getData() {
        return data;
    }

    public void setData(List<ClusterData> data) {
        this.data = data;
    }
}
