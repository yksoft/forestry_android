package com.mradmin.yks13.forestry_android.ui.main_screen;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.mradmin.yks13.forestry_android.R;
import com.mradmin.yks13.forestry_android.ui.base.BaseFragment;
import com.mradmin.yks13.forestry_android.util.MathUtils;
import com.mradmin.yks13.forestry_android.util.PermissionUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends BaseFragment implements OnMapReadyCallback {

    private static final int PERMISSION_CODE = 1010;

    @BindView(R.id.popup_container)
    RelativeLayout infoWindowContainer;
    @BindView(R.id.polygon_name)
    TextView polygonName;

    private int width, height;
    private GoogleMap map;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        mapFragment.getView().getViewTreeObserver()
//                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                    @Override
//                    public void onGlobalLayout() {
//                        width = mapFragment.getView().getWidth();
//                        height = mapFragment.getView().getHeight() + MathUtils.dpToPx(getContext(), 26);
//
//                        mapFragment.getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    }
//                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_CODE:
                prepareMap();
                //enableUserLocation();
                break;
        }
    }

    private void checkPermissions() {
        if (!PermissionUtils.hasLocationPermission(getContext())) {
            PermissionUtils.requestLocationPermission(this, PERMISSION_CODE);
            return;
        }

        prepareMap();
    }

    @SuppressWarnings({"MissingPermission"})
    private void prepareMap() {
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setRotateGesturesEnabled(false);
        map.getUiSettings().setCompassEnabled(false);
        map.getUiSettings().setMapToolbarEnabled(false);

        //map.setOnCameraMoveListener(this);
        //map.setOnMapLongClickListener(this);
        //map.setOnMapClickListener(this);
        //map.setOnPolygonClickListener(this);
        //map.setOnPolylineClickListener(this);
        //map.setOnCameraIdleListener(this);
        //map.setOnCameraMoveStartedListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        checkPermissions();
    }

}
