package com.mradmin.yks13.forestry_android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.mradmin.yks13.forestry_android.R;

public class NextButton extends android.support.v7.widget.AppCompatButton {
    private String enableText;
    private String disableText;

    public NextButton(Context context) {
        super(context);
    }

    private void prepareStyles(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.NextButton,
                0, 0);

        try {
            enableText = a.getString(R.styleable.NextButton_enableText);
            disableText = a.getString(R.styleable.NextButton_disableText);

            setText(isEnabled() ? enableText : disableText);
        } finally {
            a.recycle();
        }
    }

    public NextButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        prepareStyles(context, attrs);
    }

    public NextButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        prepareStyles(context, attrs);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        String text;

        if (enabled) {
            text = enableText;
        } else {
            text = disableText;
        }

        setText(text);
    }
}
