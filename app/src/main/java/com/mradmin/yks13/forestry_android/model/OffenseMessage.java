package com.mradmin.yks13.forestry_android.model;

import android.os.Parcel;
import android.os.Parcelable;

public class OffenseMessage implements Parcelable {

    private OffenseType type;
    private String message;
    private byte[] image;
    private Float lat;
    private Float lon;

    public OffenseType getType() {
        return type;
    }

    public void setType(OffenseType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public OffenseMessage() {
    }

    public OffenseMessage(OffenseType type, String message, Float lat, Float lon, byte[] image) {
        this.type = type;
        this.message = message;
        this.lat = lat;
        this.lon = lon;
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeString(this.message);
//        dest.writeInt(this.image == null ? -1 : this.image.length);
//        dest.writeByteArray(this.image);
        dest.writeFloat(this.lat == null ? -1 : this.lat);
        dest.writeFloat(this.lon == null ? -1 : this.lon);
    }

    public OffenseMessage(Parcel in){
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : OffenseType.values()[tmpType];
        this.message = in.readString();
//        int tmpByteLength = in.readInt();
//        this.image = tmpByteLength == -1 ? null : new byte[tmpByteLength];
//        in.readByteArray(this.image == null ? new byte[0]: this.image);
        float tmpLat = in.readFloat();
        this.lat = tmpLat == -1 ? null : tmpLat;
        float tmpLon = in.readFloat();
        this.lon = tmpLon == -1 ? null : tmpLon;
    }

    public static final Creator<OffenseMessage> CREATOR = new Creator<OffenseMessage>() {
        @Override
        public OffenseMessage createFromParcel(Parcel source) {
            return new OffenseMessage(source);
        }

        @Override
        public OffenseMessage[] newArray(int size) {
            return new OffenseMessage[size];
        }
    };

    @Override
    public String toString() {
        return "OffenseMessage{" +
                "type=" + type +
                ", message='" + message + '\'' +
                ", image=" + image +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
