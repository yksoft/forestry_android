package com.mradmin.yks13.forestry_android.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;

public class ClusterData implements ClusterItem {
    private int id;
    private OffenseType type;
    private int legal;
    @SerializedName("geo_position")
    @Expose
    private ClusterGeoPosition geoPosition;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OffenseType getType() {
        return type;
    }

    public void setType(OffenseType type) {
        this.type = type;
    }

    public int isLegal() {
        return legal;
    }

    public void setLegal(int legal) {
        this.legal = legal;
    }

    public ClusterGeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(ClusterGeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }

    public ClusterData() {
    }

    public ClusterData(int id, OffenseType type, int legal, ClusterGeoPosition geoPosition) {
        this.id = id;
        this.type = type;
        this.legal = legal;
        this.geoPosition = geoPosition;
    }

    //cluste item implementation
    @Override
    public LatLng getPosition() {
        if (getGeoPosition() != null && getGeoPosition().getCoordinates() != null && getGeoPosition().getCoordinates().size() >=2) {
            return new LatLng(getGeoPosition().getCoordinates().get(0), getGeoPosition().getCoordinates().get(1));
        } else {
            return null;
        }
    }

    @Override
    public String getTitle() {
        return type.toString();
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
