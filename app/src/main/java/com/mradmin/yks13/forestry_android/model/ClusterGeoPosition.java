package com.mradmin.yks13.forestry_android.model;

import java.util.List;

public class ClusterGeoPosition {
    private String type;
    private List<Float> coordinates;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Float> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Float> coordinates) {
        this.coordinates = coordinates;
    }

    public ClusterGeoPosition() {
    }

    public ClusterGeoPosition(String type, List<Float> coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }
}
