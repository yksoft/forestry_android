package com.mradmin.yks13.forestry_android.model;

import java.util.List;

public class MapLayer {

    private String id;
    private String name;
    private String district;
    private List<Double> center;

    public MapLayer() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public List<Double> getCenter() {
        return center;
    }

    public void setCenter(List<Double> center) {
        this.center = center;
    }

    public double getLatitude() {
        if (center != null && center.size() == 2) {
            return center.get(1);
        }

        return 0;
    }

    public double getLongitude() {
        if (center != null && center.size() == 2) {
            return center.get(0);
        }

        return 0;
    }

    public static String createId(String latitude, String longitude) throws Exception {
        return latitude.replaceAll("\\.", "").concat(longitude.replaceAll("\\.", ""));
    }

    @Override
    public String toString() {
        return "MapLayer{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", district='" + district + '\'' +
                ", center=" + center +
                '}';
    }
}
